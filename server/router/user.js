const express = require('express')
const Result = require('../models/result')
const { body, validationResult } = require('express-validator')
const  boom  =  require('boom')
const { querySqlAll, querySqlOne, insertSql, insertImportKey } = require('../db/index')
const { NETTYPE } = require('../utils/config');
const { Wallet, WiccApi, WaykiTransaction } = require("wicc-wallet-lib")

const router = express.Router()

var log = require('log4js').getLogger("user");

//测试
router.get('/info', function(req, res, next) {
    new  Result({  data: 'get请求成功' }, 'success').success(res)    

})


router.get('/addresslist', function(req, res, next) {
    querySqlAll('select id,address,mnemonic_offset,status,created_at from wallet where status = 200').then(reswallet => { 
        new  Result({  data: reswallet }, 'success').success(res)           
    }).catch(err  =>  {        
        //console.log(err)  
        log.error(err);  
    })    

})


router.get('/coinlist', function(req, res, next) {
    querySqlAll('select * from coin').then(rescoin => { 
        new  Result({  data: rescoin }, 'success').success(res)           
    }).catch(err  =>  {        
        // console.log(err) 
        log.error(err);   
    })    

})


router.post(
    '/transfertx', [
        body('nValidHeight').isNumeric().withMessage('区块高度类型不正确'),
        body('fees').isNumeric().withMessage('矿工费类型不正确')
    ],
    function(req, res, next) {
        const err = validationResult(req)
        if (!err.isEmpty()) {
            const [{ msg }] = err.errors //通过解构的方式拿到msg
                //错误信息借助boom实现
            next(boom.badRequest(msg)) //badRequest 参数异常
        } else {
            let { nTxType, nValidHeight, fees, dests, memo, feeSymbol, addressValue } = req.body

            querySqlOne('select * from wallet where address=?', addressValue).then(raddressitem => { 
                //网络初始化
                var arg = { network: NETTYPE }
                var wiccApi = new WiccApi(arg)

                if (raddressitem.priv_key) { //有私钥时
                    var privKeyWIF = raddressitem.priv_key
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    let cointransferTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        //srcRegId: publickey, //When srcRegId is available, use RegID as priority since it cost 0.001 WICC trx fees otherwise it'll be 0.002 WICC
                        dests: dests,
                        memo: memo, // remark, optional field
                        feeSymbol: feeSymbol
                    };

                    let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res) 
                } else {
                    querySqlOne('select * from mnemonic where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                        let strMne = resmnemonic.mnemonic
                        var walletInfo = wiccApi.createWallet(strMne, '12345678')
                        var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                        var publickey = raddressitem.pub_key
                        let wallet = new Wallet(privKeyWIF)

                        let cointransferTxinfo = {
                            nTxType: nTxType,
                            nValidHeight: nValidHeight,
                            fees: fees,
                            //srcRegId: publickey, //When srcRegId is available, use RegID as priority since it cost 0.001 WICC trx fees otherwise it'll be 0.002 WICC
                            dests: dests,
                            memo: memo, // remark, optional field
                            feeSymbol: feeSymbol
                        };

                        let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                        let cointransferTx = transaction.genRawTx()
                        new  Result({  data: cointransferTx }, 'success').success(res)    

                    }).catch(err  =>  {  
                        log.error(err);      
                        //console.log(err)    
                    })         
                }

            }).catch(err  =>  { 
                log.error(err);       
                //console.log(err)    
            })    

        }
    })


router.post(
    '/create',
    function(req, res) {
        let { addressArr } = req.body
            //console.log("addressArr=>", addressArr)
        insertSql(addressArr).then(insert => {
            // console.log('==>', insert)
            new  Result('success').success(res)

        }).catch(err  =>  {        
            log.error(err);
            res.json({ code: 9000, msg: '插入数据异常' })

        })  

    })


router.post(
    '/importkey',
    function(req, res) {
        let { importKeyObj } = req.body

        insertImportKey(importKeyObj).then(insert => {
            if (insert.code === 100) {
                new  Result('该账户已存在，无需重复导入').existed(res)
            } else {
                new  Result('success').success(res)
            }
        }).catch(err  =>  {        
            log.error(err);
            res.json({ code: 9000, msg: '插入数据异常' })

        })  

    })


router.post(
    '/votetx',
    function(req, res) {

        let { nTxType, nValidHeight, fees, voteLists, memo, addressValue } = req.body

        querySqlOne('select * from wallet where address=?', addressValue).then(raddressitem => { 
            //网络初始化
            var arg = { network: NETTYPE }
            var wiccApi = new WiccApi(arg)

            if (raddressitem.priv_key) { //有私钥时
                var privKeyWIF = raddressitem.priv_key
                var publickey = raddressitem.pub_key
                let wallet = new Wallet(privKeyWIF)

                let cointransferTxinfo = {
                    nTxType: nTxType,
                    nValidHeight: nValidHeight,
                    fees: fees,
                    voteLists: voteLists
                };

                let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                let cointransferTx = transaction.genRawTx()
                new  Result({  data: cointransferTx }, 'success').success(res) 
            } else {
                querySqlOne('select * from mnemonic where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                    let strMne = resmnemonic.mnemonic
                    var walletInfo = wiccApi.createWallet(strMne, '12345678')
                    var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    let cointransferTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        voteLists: voteLists
                    };

                    let transaction = new WaykiTransaction(cointransferTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res)    

                }).catch(err  =>  {   
                    log.error(err);     
                    //console.log(err)    
                })         
            }

        }).catch(err  =>  {     
            log.error(err);   
            //console.log(err)    
        })    


    })


router.post(
    '/cdptx',
    function(req, res) {

        let { nTxType, nValidHeight, cdpTxId, fees, feeSymbol, memo, addressValue, assetAmount, sCoinToMint } = req.body

        querySqlOne('select * from wallet where address=?', addressValue).then(raddressitem => { 
            //网络初始化
            var arg = { network: NETTYPE }
            var wiccApi = new WiccApi(arg)

            if (raddressitem.priv_key) { //有私钥时
                var privKeyWIF = raddressitem.priv_key
                var publickey = raddressitem.pub_key
                let wallet = new Wallet(privKeyWIF)

                var assetSymbol = "WICC"
                var map = new Map([
                    [assetSymbol, assetAmount]
                ])
                let cdpStakeTxinfo = {
                    nTxType: nTxType,
                    nValidHeight: nValidHeight,
                    fees: fees,
                    feeSymbol: feeSymbol,
                    cdpTxId: cdpTxId,
                    assetMap: map,
                    sCoinSymbol: "WUSD",
                    sCoinToMint: sCoinToMint
                };

                let transaction = new WaykiTransaction(cdpStakeTxinfo, wallet)
                let cointransferTx = transaction.genRawTx()
                new  Result({  data: cointransferTx }, 'success').success(res) 
            } else {
                querySqlOne('select * from mnemonic where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                    let strMne = resmnemonic.mnemonic
                    var walletInfo = wiccApi.createWallet(strMne, '12345678')
                    var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    var assetSymbol = "WICC"
                    var map = new Map([
                        [assetSymbol, assetAmount]
                    ])
                    let cdpStakeTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        feeSymbol: feeSymbol,
                        cdpTxId: cdpTxId,
                        assetMap: map,
                        sCoinSymbol: "WUSD",
                        sCoinToMint: sCoinToMint
                    };
                    //console.log(cdpStakeTxinfo)
                    let transaction = new WaykiTransaction(cdpStakeTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res)     

                }).catch(err  =>  {  
                    log.error(err);      
                    //console.log(err)    
                })         
            }

        }).catch(err  =>  {    
            log.error(err);    
            //console.log(err)    
        })    


    })


router.post(
    '/cdpredeemtx',
    function(req, res) {

        let { nTxType, nValidHeight, cdpTxId, fees, feeSymbol, memo, addressValue, assetAmount, sCoinsToRepay } = req.body

        querySqlOne('select * from wallet where address=?', addressValue).then(raddressitem => { 
            //网络初始化
            var arg = { network: NETTYPE }
            var wiccApi = new WiccApi(arg)

            if (raddressitem.priv_key) { //有私钥时
                var privKeyWIF = raddressitem.priv_key
                var publickey = raddressitem.pub_key
                let wallet = new Wallet(privKeyWIF)

                var assetSymbol = "WICC"
                var map = new Map([
                    [assetSymbol, assetAmount]
                ])
                let cdpRedeemTxinfo = {
                    nTxType: nTxType,
                    nValidHeight: nValidHeight,
                    fees: fees,
                    cdpTxId: cdpTxId,
                    feeSymbol: feeSymbol,
                    assetMap: map,
                    sCoinsToRepay: sCoinsToRepay,
                };

                let transaction = new WaykiTransaction(cdpRedeemTxinfo, wallet)
                let cointransferTx = transaction.genRawTx()
                new  Result({  data: cointransferTx }, 'success').success(res) 
            } else {
                querySqlOne('select * from mnemonic where id=?', raddressitem.mnemonic_id).then(resmnemonic => { 

                    let strMne = resmnemonic.mnemonic
                    var walletInfo = wiccApi.createWallet(strMne, '12345678')
                    var privKeyWIF = wiccApi.getHDPriKeyFromSeed(walletInfo.seedinfo, '12345678', raddressitem.mnemonic_offset)
                    var publickey = raddressitem.pub_key
                    let wallet = new Wallet(privKeyWIF)

                    var assetSymbol = "WICC"
                    var map = new Map([
                        [assetSymbol, assetAmount]
                    ])
                    let cdpRedeemTxinfo = {
                        nTxType: nTxType,
                        nValidHeight: nValidHeight,
                        fees: fees,
                        cdpTxId: cdpTxId,
                        feeSymbol: feeSymbol,
                        assetMap: map,
                        sCoinsToRepay: sCoinsToRepay,
                    };

                    let transaction = new WaykiTransaction(cdpRedeemTxinfo, wallet)
                    let cointransferTx = transaction.genRawTx()
                    new  Result({  data: cointransferTx }, 'success').success(res)     

                }).catch(err  =>  {  
                    log.error(err);      
                    //console.log(err)    
                })         
            }

        }).catch(err  =>  {     
            log.error(err);   
            //console.log(err)    
        })    


    })
module.exports = router