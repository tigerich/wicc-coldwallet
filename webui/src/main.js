// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import api from "./tools/api";
import {
    Button,
    Select,
    Option,
    Input,
    Dialog,
    Message,
    Tabs,
    TabPane,
    Loading,
    Icon,
    Collapse,
    CollapseItem,
    Tooltip
} from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(Button)
Vue.use(Select)
Vue.use(Option)
Vue.use(Input)
Vue.use(Dialog)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Loading.directive);
Vue.use(Icon);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Tooltip);

Vue.prototype.$loading = Loading.service;
Vue.component(Message)

Vue.prototype.$http = api
Vue.prototype.$message = Message
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})